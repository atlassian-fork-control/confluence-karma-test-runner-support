package com.atlassian.confluence.test.support.karma;

import java.util.List;
import java.util.Set;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.google.template.soy.data.SoyData;
import com.google.template.soy.jssrc.restricted.JsExpr;
import com.google.template.soy.jssrc.restricted.SoyJsSrcFunction;
import com.google.template.soy.tofu.restricted.SoyTofuFunction;

public class DarkFeatureFunction implements SoyJsSrcFunction, SoyTofuFunction
{

    private static List<String> enableDarkFeatures;

    static
    {
        String darkFeatureSetting = System.getProperty("dark.feature.enable");
        if (null == darkFeatureSetting)
        {
            darkFeatureSetting = "";
        }
        enableDarkFeatures = Lists.asList(null, darkFeatureSetting.split(","));
    }

    @Override
    public String getName()
    {
        return "isDarkFeatureEnabled";
    }

    @Override
    public Set<Integer> getValidArgsSizes()
    {
        final Set<Integer> ret = Sets.newHashSet();
        ret.add(1);
        return ret;
    }

    @Override
    public SoyData computeForTofu(final List<SoyData> args)
    {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public JsExpr computeForJsSrc(final List<JsExpr> args)
    {
        if (enableDarkFeatures.contains(args.get(0).getText()))
        {
            return new JsExpr("true", 0);
        }
        return new JsExpr("false", 0);
    }

}
