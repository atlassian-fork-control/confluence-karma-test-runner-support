package com.atlassian.confluence.test.support.karma;

import java.util.List;

import com.atlassian.soy.spi.web.WebContextProvider;

import com.google.template.soy.data.SoyData;
import com.google.template.soy.data.restricted.StringData;
import com.google.template.soy.internal.base.CharEscaper;
import com.google.template.soy.internal.base.CharEscapers;
import com.google.template.soy.jssrc.restricted.JsExpr;

public class ContextPathFunction extends com.atlassian.soy.impl.functions.ContextFunction
{

    public ContextPathFunction(final WebContextProvider webContextProvider)
    {
        super(webContextProvider);
    }

    public ContextPathFunction()
    {
        super(null);
    }

    @Override
    public JsExpr computeForJsSrc(final List<JsExpr> args)
    {
        final CharEscaper jsEscaper = CharEscapers.javascriptEscaper();
        return new JsExpr('"' + jsEscaper.escape(getContextPath()) + '"', Integer.MAX_VALUE);
    }

    @Override
    public SoyData computeForTofu(final List<SoyData> soyData)
    {
        return StringData.forValue(getContextPath());
    }

    private static String getContextPath()
    {
        String path = System.getProperty("context.path");
        if (path == null)
        {
            path = "/confluence";
        }
        return path;
    }

}
