package com.atlassian.confluence.test.support.karma;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.google.common.collect.Lists;
import com.google.template.soy.data.SoyData;
import com.google.template.soy.jssrc.restricted.JsExpr;
import com.google.template.soy.jssrc.restricted.SoyJsSrcFunction;
import com.google.template.soy.tofu.restricted.SoyTofuFunction;

public class InlineCommentDarkFeatureFunction implements SoyJsSrcFunction, SoyTofuFunction
{

    private static final Set<Integer> argSizes = new HashSet<Integer>(2);
    private static List<String> enableDarkFeatures;

    static
    {
        String darkFeatureSetting = System.getProperty("dark.feature.enable");
        if (null == darkFeatureSetting)
        {
            darkFeatureSetting = "";
        }
        enableDarkFeatures = Lists.asList(null, darkFeatureSetting.split(","));
        System.out.println("InlineCommentDarkFeatureFunction.enclosing_method() " + enableDarkFeatures);
        System.out.println("InlineCommentDarkFeatureFunction.enclosing_method() " + enableDarkFeatures.size());
    }

    public InlineCommentDarkFeatureFunction()
    {
        argSizes.add(0);
        argSizes.add(1);
    }

    @Override
    public String getName()
    {
        return "isInlineCommentsEnabled";
    }

    @Override
    public Set<Integer> getValidArgsSizes()
    {
        return argSizes;
    }

    @Override
    public SoyData computeForTofu(final List<SoyData> args)
    {
        return null;
    }

    @Override
    public JsExpr computeForJsSrc(final List<JsExpr> args)
    {
        if (args.size() == 0)
        {
            return new JsExpr("true", 0);
        }
        final String key = getDarkFeatureKey(args.get(0).getText());
        if (enableDarkFeatures.contains(key))
        {
            return new JsExpr("true", 0);
        }
        return new JsExpr("false", 0);
    }

    private String getDarkFeatureKey(final String key) {
        String retKey = key;
        if (retKey.startsWith("'"))
        {
            retKey = retKey.substring(1);
        }
        if (retKey.endsWith("'"))
        {
            retKey = retKey.substring(0, retKey.length() - 1);
        }
        return retKey;
    }

}
